namespace Repositorios.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            
            CreateTable(
                "dbo.Factura",
                c => new
                    {
                        Numero = c.String(nullable: false, maxLength: 50),
                        Fechaemision = c.DateTime(nullable: false),
                        Fechavencimiento = c.DateTime(),
                        Fechadecobro = c.DateTime(),
                        Ruccliente = c.Int(),
                        RazonSocial = c.String(maxLength: 100),
                        TotalFactura = c.Decimal(precision: 18, scale: 4),
                        Totalimpuestoley = c.Decimal(precision: 18, scale: 4),
                        Facturaescaneada = c.String(maxLength: 50),
                        Empresa = c.Int(),
                    })
                .PrimaryKey(t => t.Numero);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Factura");
        }
    }
}
