namespace Repositorios.Factura
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Factura")]
    public partial class Factura
    {
        [Key]
        [StringLength(50)]
        public string Numero { get; set; }

        public DateTime Fechaemision { get; set; }

        public DateTime? Fechavencimiento { get; set; }

        public DateTime? Fechadecobro { get; set; }

        public int? Ruccliente { get; set; }

        [StringLength(100)]
        public string RazonSocial { get; set; }

        public decimal? TotalFactura { get; set; }

        public decimal? Totalimpuestoley { get; set; }

        [StringLength(50)]
        public string Facturaescaneada { get; set; }

        public int? Empresa { get; set; }
    }
}
