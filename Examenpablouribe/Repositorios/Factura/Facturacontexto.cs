namespace Repositorios.Factura
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Facturacontexto : DbContext
    {
        public Facturacontexto()
            : base("name=Facturacontex")
        {
        }

        public virtual DbSet<Factura> Factura { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Factura>()
                .Property(e => e.TotalFactura)
                .HasPrecision(18, 4);

            modelBuilder.Entity<Factura>()
                .Property(e => e.Totalimpuestoley)
                .HasPrecision(18, 4);
        }
    }
}
